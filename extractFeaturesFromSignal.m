function [peakFreq, meanFreq] = extractSignalFeatures(signalSegment, Fs)
    N = length(signalSegment);
    fftSegment = fft(signalSegment);
    magnitude = abs(fftSegment/N);
    magnitude = magnitude(1:floor(N/2)+1); % Single-sided spectrum
    frequency = (0:length(magnitude)-1)*(Fs/N);
    
    % Peak frequency
    [~, indexMax] = max(magnitude);
    peakFreq = frequency(indexMax); % This should be a scalar
    
    % % Spectral centroid
    % % spectralCentroid = spectralCentroid(fftSegment, Fs)
    % % Calculate the power spectral density (PSD) using Welch's method
    % [pxx, f] = pwelch(signalSegment, [], [], [], Fs);
    % 
    % % Find the indices of frequencies that are within the desired band
    % freqBand = [20 500];
    % % bandIndices = f >= freqBand(1) & f <= freqBand(2);
    % 
    % % Calculate the band power
    % bp = bandpower(pxx, f, freqBand, 'psd');
    
    % If you want to use the power of the signal directly (not PSD), you would sum the squared magnitudes
    % of the frequencies within the band, as shown below:
    % bp = sum(pxx(bandIndices));
    [P1, f] = pwelch(signalSegment, [], [], [], Fs);

    % Find peaks in the PSD
    [~, ~] = findpeaks(P1, f, 'MinPeakProminence',0.001, 'MinPeakDistance',0.1);


    % Feature 2: mean frequency, weighted by power
    meanFreq = sum(f.*P1)/sum(P1);
end