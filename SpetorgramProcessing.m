[SOIgood, SOIbad] = readData();
%%
SOIgood = table2array(SOIgood);
SOIbad = table2array(SOIbad);
%%

%%
Fs = 48000; % Sampling frequency
windowSize = 1024; % Size of window to analyze
overlap = 512; % Overlap between windows



% Preallocate feature arrays
numWindows = floor((length(SOIgood)-overlap)/(windowSize-overlap));
featuresGood = zeros(numWindows, 2); % Assuming we're extracting 2 features
featuresBad = zeros(numWindows, 2);


% Extract features for good bearings
for i = 1:numWindows
    startIdx = (i-1)*(windowSize-overlap)+1;
    endIdx = startIdx+windowSize-1;
    [peakFreq, meanFreq] = extractFeatures2(SOIgood(startIdx:endIdx), Fs);
    featuresGood(i, :) = [peakFreq, meanFreq];
end

% Repeat feature extraction for bad bearings
for i = 1:numWindows
    startIdx = (i-1)*(windowSize-overlap)+1;
    endIdx = startIdx+windowSize-1;
    [peakFreq, meanFreq] = extractFeatures2(SOIbad(startIdx:endIdx), Fs);
    featuresBad(i, :) = [peakFreq, meanFreq];
end

labelsGood = zeros(size(featuresGood, 1), 1); % Create a column of 0's for good bearings
labelsBad = ones(size(featuresBad, 1), 1); % Create a column of 1's for bad bearings

% Combine the labels
allLabels = [labelsGood; labelsBad];
% Combine good and bad features for clustering
allFeatures = [featuresGood; featuresBad];

% Define a function to extract your desired features
% Here, as an example, we use peak frequency and spectral centroid
%%
% Normalize the features
minVals = min(allFeatures);
maxVals = max(allFeatures);
normalizedFeatures = (allFeatures - minVals) ./ (maxVals - minVals);

%%
% Assuming an 80-20 split for training and testing
numTraining = floor(0.8 * size(normalizedFeatures, 1));
numTesting = size(normalizedFeatures, 1) - numTraining;

% Create training and testing datasets
indices = randperm(size(normalizedFeatures, 1));
trainingData = normalizedFeatures(indices(1:numTraining), :);
testingData = normalizedFeatures(indices(numTraining+1:end), :);


% Remember to keep track of which windows correspond to good and bad bearings
trainingLabels = indices(1:numTraining) > numWindows; % true if bad, false if good
testingLabels = indices(numTraining+1:end) > numWindows; % true if bad, false if good
%%
% 'features' is a matrix where each row represents a spectral diagram
% and each column is a feature extracted from that diagram
numClusters = 2; % for example, 'working' and 'with defects'

% Run k-means
[idx, C] = kmeans(trainingData, numClusters);

% 'idx' contains cluster indices of each observation
% 'C' is the location of centroids
%%
% 'features' as defined above
%dimensions = [somWidth somHeight]; % Define the dimensions of the SOM

% Train Self-Organizing Map
net = selforgmap();
net = train(net, trainingData');
%%

% View the SOM

% Get the winning neurons for each feature vector
winningNeurons = vec2ind(net(testingData));
%%
predictedLabels = mapNeuronToLabel(winningNeurons);
[C, order] = confusionmat(double(testingLabels), predictedLabels);
%%
%%
% Calculate Accuracy
accuracy = sum(diag(C)) / sum(C, 'all');

% Precision, Recall, and F1-Score (for a binary classification)
precision = C(1,1) / (C(1,1) + C(2,1));
recall = C(1,1) / (C(1,1) + C(1,2));
f1Score = 2 * (precision * recall) / (precision + recall);


[~,idx_test] = pdist2(C,testingData,'euclidean','Smallest',1);



%%
function [peakFreq, spectralCentroid] = extractFeatures(signalSegment, Fs)
    fftSegment = fft(signalSegment);
    magnitude = abs(fftSegment);
    magnitude = magnitude(1:ceil(end/2)); % Symmetry in FFT
    frequency = (0:length(magnitude)-1)*(Fs/length(signalSegment));
    
    % Peak frequency
    [~, idx] = max(magnitude);
    peakFreq = frequency(idx);
    
    % Spectral centroid
    spectralCentroid = sum(frequency .* magnitude) / sum(magnitude);
end
function [peakFreq, meanFreq] = extractFeatures2(signalSegment, Fs)
    N = length(signalSegment);
    fftSegment = fft(signalSegment);
    magnitude = abs(fftSegment/N);
    magnitude = magnitude(1:floor(N/2)+1); % Single-sided spectrum
    frequency = (0:length(magnitude)-1)*(Fs/N);
    
    % Peak frequency
    [~, indexMax] = max(magnitude);
    peakFreq = frequency(indexMax); % This should be a scalar
    
    % % Spectral centroid
    % % spectralCentroid = spectralCentroid(fftSegment, Fs)
    % % Calculate the power spectral density (PSD) using Welch's method
    % [pxx, f] = pwelch(signalSegment, [], [], [], Fs);
    % 
    % % Find the indices of frequencies that are within the desired band
    % freqBand = [20 500];
    % % bandIndices = f >= freqBand(1) & f <= freqBand(2);
    % 
    % % Calculate the band power
    % bp = bandpower(pxx, f, freqBand, 'psd');
    
    % If you want to use the power of the signal directly (not PSD), you would sum the squared magnitudes
    % of the frequencies within the band, as shown below:
    % bp = sum(pxx(bandIndices));
    [P1, f] = pwelch(signalSegment, [], [], [], Fs);

    % Find peaks in the PSD
    [~, ~] = findpeaks(P1, f, 'MinPeakProminence',0.001, 'MinPeakDistance',0.1);


    % Feature 2: mean frequency, weighted by power
    meanFreq = sum(f.*P1)/sum(P1);
end

function [peakFreq, meanFreq] = defineFreqPWELCH(SOI)
    Fs = 1000; % Sampling frequency
    t = 0:1/Fs:1-1/Fs; % Time vector 
    signal = SOI;
    
    % Estimate power spectral density
    [P1, f] = pwelch(signal, [], [], [], Fs);

    % Find peaks in the PSD
    [pks, locs] = findpeaks(P1, f, 'MinPeakProminence',0.001, 'MinPeakDistance',0.1);

    % Feature 1: peak frequency
    if isempty(locs)
        peakFreq = 0; % If no peaks are found, set to 0
    else
        peakFreq = locs(1); % Use the first peak
    end

    % Feature 2: mean frequency, weighted by power
    meanFreq = sum(f.*P1)/sum(P1);

    % FFT of the signal
    % fftSignal = fft(signal)
    % P2 = abs(fftSignal/length(signal));
    % P1 = P2(1:length(signal)/2+1);
    % P1(2:end-1) = 2*P1(2:end-1);
    % 
    % % Frequency vector
    % f = Fs*(0:(length(signal)/2))/length(signal);
    % 
    % % Extract features: peak frequency and mean frequency
    % [peakAmp, peakIdx] = max(P1);
    % peakFreq = f(peakIdx);
    % meanFreq = sum(f.*P1)/sum(P1);
end
%% 
% 

function label = mapNeuronToLabel(neuronIndex)
    % Define your logic here. For example:
    % if neuronIndex in some range, then label = 1 (good)
    % else label = 0 (bad)
    % This is a placeholder logic.
    label = randi([0, 1], 1, length(neuronIndex)); % Placeholder, replace with actual logic
end

function [SOIgood, SOIbad] = readData(goodFilePath, badFilePath)
%READDATA Summary of this function goes here

    opts = delimitedTextImportOptions("NumVariables", 1);
    
    % Specify range and delimiter
    opts.DataLines = [1, Inf];
    opts.Delimiter = "\t";
    
    % Specify column names and types
    opts.VariableNames = "e01";
    opts.VariableTypes = "double";
    
    % Specify file level properties
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    opts.ConsecutiveDelimitersRule = "join";
    
    % Import the data 1
    SOIgood = readtable(goodFilePath, opts);
    SOIbad = readtable(badFilePath, opts);

    clear opts
end

