function [SOIgood,SOIbad] = readData()
%READDATA Summary of this function goes here

    opts = delimitedTextImportOptions("NumVariables", 1);
    
    % Specify range and delimiter
    opts.DataLines = [1, Inf];
    opts.Delimiter = "\t";
    
    % Specify column names and types
    opts.VariableNames = "e01";
    opts.VariableTypes = "double";
    
    % Specify file level properties
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    opts.ConsecutiveDelimitersRule = "join";
    
    % Import the data 1
    SOIgood = readtable("/Users/glibglugovskiy/Projects/AUK/UNI/capstone/MATLAB Unsupervised/SOI_good.txt", opts);
    SOIbad = readtable("/Users/glibglugovskiy/Projects/AUK/UNI/capstone/MATLAB Unsupervised/SOI_bad.txt", opts);

    clear opts
end

